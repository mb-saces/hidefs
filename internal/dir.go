package hidefs

import (
	"context"
	"log"
	"os"
	"path/filepath"
	"sync"
	"syscall"

	"bazil.org/fuse"
	fusefs "bazil.org/fuse/fs"
	"gitlab.com/mb-saces/hidefs/goutensil"
)

var _ fusefs.Node = (*Dir)(nil)
var _ fusefs.NodeCreater = (*Dir)(nil)
var _ fusefs.NodeMkdirer = (*Dir)(nil)
var _ fusefs.NodeRemover = (*Dir)(nil)
var _ fusefs.NodeRenamer = (*Dir)(nil)
var _ fusefs.NodeStringLookuper = (*Dir)(nil)

//var _ fs.NodeSetattrer = (*Dir)(nil)

type Dir struct {
	sync.RWMutex
	attr fuse.Attr

	path         string
	fs           *HideFS
	autoHideList goutensil.HashList
}

func (d *Dir) Attr(ctx context.Context, o *fuse.Attr) error {
	d.RLock()
	*o = d.attr
	d.RUnlock()
	return nil
}

/*
func (d *Dir) Setattr(ctx context.Context, req *fuse.SetattrRequest, resp *fuse.SetattrResponse) error {
	fmt.Printf("TODO Dir.Settattr(): %+v\n", req)
	fmt.Printf("TODO Dir.Settattr(): %s\n", req.Valid.String())
	return nil
}
*/

func (d *Dir) Lookup(ctx context.Context, name string) (fusefs.Node, error) {
	if d.isHidden(name) {
		return nil, syscall.ENOENT
	}
	//fmt.Printf("Lookup for: %s\n", name)
	d.RLock()
	defer d.RUnlock()
	// n, exist := d.nodes[name]
	path := filepath.Join(d.path, name)
	stats, err := os.Stat(path)
	if err != nil {
		//The real file does not exists.
		// log.Println("Lookup ERR: ", err)
		return nil, syscall.ENOENT
	}

	// log.Println("Lookup", path)

	switch {
	case stats.IsDir():
		// log.Printf("DRIVE | DIR LOOKUP: %s\n", virtualPath)
		return d.fs.newDirFileInfo(path, stats), nil
	case stats.Mode().IsRegular():
		return d.fs.newFile(path, stats), nil
	default:
		panic("Unknown type in filesystem")
	}
}

func (d *Dir) ReadDirAll(ctx context.Context) ([]fuse.Dirent, error) {
	d.RLock()
	defer d.RUnlock()

	//log.Println("ReadDirAll ", d.path)

	mkDirent := func(name string, isDir bool) (de fuse.Dirent) {
		de.Name = name
		if isDir {
			de.Type = fuse.DT_Dir
		} else {
			de.Type = fuse.DT_File
		}
		return
	}

	files, err := os.ReadDir(d.path)
	if err != nil {
		log.Println("ReadDirAll ERR: ", err)
		return nil, err
	}
	out := make([]fuse.Dirent, 0, len(files)+2)

	_, err = os.Lstat(d.path + "/.")

	//fmt.Printf("DEBUG->:  %s\n", fileInfo.Name())
	if err != nil {
		log.Println("ReadDirAll ERR - no '.': ", err)
	} else {
		out = append(out, mkDirent(".", true))
	}

	//fileInfo, err = os.Lstat(path.Join(d.path, ".."))
	_, err = os.Lstat(d.path + "/..")

	//fmt.Printf("DEBUG->:  %s\n", fileInfo.Name())
	if err != nil {
		log.Println("ReadDirAll ERR - no '..': ", err)
	} else {
		out = append(out, mkDirent("..", true))
	}

	for _, node := range files {
		// autoHideList is not populated yet, so lookup global list directly
		if d.fs.isHidden(node.Name()) {
			continue
		}
		if node.IsDir() {
			out = append(out, mkDirent(node.Name(), true))
		} else if node.Type().IsRegular() {
			out = append(out, mkDirent(node.Name(), false))
		} else {
			log.Printf("Not a regular file: '%s', adding to autoignore", node.Name())
			d.autoHideList.Add(node.Name())
		}
	}

	return out, nil
}

func (d *Dir) Mkdir(ctx context.Context, req *fuse.MkdirRequest) (fusefs.Node, error) {
	d.Lock()
	defer d.Unlock()

	//log.Println(req)
	if d.isHidden(req.Name) {
		return nil, syscall.EPERM
	}

	if exists := d.exists(req.Name); exists {
		log.Println("Mkdir ERR: EEXIST")
		return nil, syscall.EEXIST
	}

	path := filepath.Join(d.path, req.Name)
	n := d.fs.newRootDir(path, req.Mode)

	if err := os.Mkdir(path, req.Mode); err != nil {
		log.Println("Mkdir ERR:  ", err)
		return nil, err
	}
	return n, nil
}

func (d *Dir) Create(ctx context.Context, req *fuse.CreateRequest, resp *fuse.CreateResponse) (fusefs.Node, fusefs.Handle, error) {
	if d.isHidden(req.Name) {
		return nil, nil, syscall.EPERM
	}

	d.Lock()
	defer d.Unlock()

	//log.Println(req)
	if exists := d.exists(req.Name); exists {
		log.Println("Create open ERR: EEXIST")
		return nil, nil, syscall.EEXIST
	}
	// log.Println(req.Mode, DEF_MODE, req.Flags)
	path := filepath.Join(d.path, req.Name)
	fHandler, err := os.OpenFile(path, int(req.Flags), req.Mode)
	if err != nil {
		log.Println("Create open ERR: ", err)
		return nil, nil, err
	}

	stat, err := fHandler.Stat()
	if err != nil {
		log.Println("Create open ERR: ", err)
		return nil, nil, err
	}
	n := d.fs.newFile(path, stat)
	n.fs = d.fs
	n.handler = fHandler

	resp.Attr = n.attr

	return n, n, nil
}

func (d *Dir) Rename(ctx context.Context, req *fuse.RenameRequest, newDir fusefs.Node) error {
	if d.isHidden(req.OldName) || d.isHidden(req.NewName) {
		return syscall.EPERM
	}

	//log.Println(req)

	nd := newDir.(*Dir)

	if d.attr.Inode == nd.attr.Inode {
		d.Lock()
		defer d.Unlock()
	} else if d.attr.Inode < nd.attr.Inode {
		d.Lock()
		defer d.Unlock()
		nd.Lock()
		defer nd.Unlock()
	} else {
		nd.Lock()
		defer nd.Unlock()
		d.Lock()
		defer d.Unlock()
	}

	if exists := d.exists(req.OldName); !exists {
		log.Println("Rename ERR: ENOENT")
		return syscall.ENOENT
	}

	oldPath := filepath.Join(d.path, req.OldName)
	newPath := filepath.Join(nd.path, req.NewName)

	if err := os.Rename(oldPath, newPath); err != nil {
		log.Println("Rename ERR: ", err)
		return err
	}
	return nil
}

func (d *Dir) Remove(ctx context.Context, req *fuse.RemoveRequest) error {
	if d.isHidden(req.Name) {
		return syscall.EPERM
	}

	d.Lock()
	defer d.Unlock()
	log.Println(req, filepath.Base(d.path), req.Name)

	if exists := d.exists(req.Name); !exists {
		log.Println("Remove ERR: ENOENT")
		return syscall.ENOENT
	}
	// else if hasChildren() {
	// 	log.Println("Remove ERR: ENOENT")
	// 	return fuse.ENOENT
	// }

	path := filepath.Join(d.path, req.Name)
	if err := os.Remove(path); err != nil {
		log.Println("Remove ERR: ", err)
		return err
	}
	return nil
}

func (d *Dir) exists(name string) bool {
	path := filepath.Join(d.path, name)
	_, err := os.Stat(path)
	return err == nil
}

func (d *Dir) isHidden(name string) bool {
	if d.fs.isHidden(name) {
		return true
	}
	return d.autoHideList.Contains(name)
}
