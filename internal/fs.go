package hidefs

import (
	"context"
	"log"
	"os"
	"sync/atomic"
	"syscall"
	"time"

	"bazil.org/fuse"
	fusefs "bazil.org/fuse/fs"
	"gitlab.com/mb-saces/hidefs/goutensil"
)

type HideFS struct {
	root               *Dir
	nodeId             uint64
	path               string
	globalNameHideList goutensil.HashList
}

// Compile-time interface checks.
var _ fusefs.FS = (*HideFS)(nil)
var _ fusefs.FSStatfser = (*HideFS)(nil)

const DEF_MODE = os.FileMode(int(0777))

func NewHideFS(path string, hidelist []string) *HideFS {
	fs := &HideFS{
		path:               path,
		globalNameHideList: make(goutensil.HashList, len(hidelist)),
	}
	for _, s := range hidelist {
		fs.globalNameHideList.Add(s)
	}
	stats, err := os.Stat(path)
	goutensil.MaybePanic(err)

	fs.root = fs.newDirFileInfo(path, stats)
	if fs.root.attr.Inode != 1 {
		panic("Root node should have been assigned id 1")
	}
	return fs
}

func (m *HideFS) isHidden(name string) bool {
	return m.globalNameHideList.Contains(name)
}

func (m *HideFS) nextId() uint64 {
	return atomic.AddUint64(&m.nodeId, 1)
}

func (m *HideFS) newDirFileInfo(path string, info os.FileInfo) *Dir {
	stat := info.Sys().(*syscall.Stat_t)
	return &Dir{
		attr: fuse.Attr{
			Valid:     500 * time.Millisecond,
			Inode:     m.nextId(),
			Size:      uint64(stat.Size),
			Blocks:    uint64(stat.Blocks),
			Atime:     time.Unix(stat.Atim.Sec, stat.Atim.Nsec),
			Mtime:     time.Unix(stat.Mtim.Sec, stat.Mtim.Nsec),
			Ctime:     time.Unix(stat.Ctim.Sec, stat.Ctim.Nsec),
			Mode:      os.ModeDir | info.Mode(),
			Nlink:     uint32(stat.Nlink),
			Uid:       stat.Uid,
			Gid:       stat.Gid,
			Rdev:      uint32(stat.Rdev),
			BlockSize: uint32(stat.Blksize),
			Flags:     0,
		},
		path:         path,
		fs:           m,
		autoHideList: make(goutensil.HashList),
	}
}

func (m *HideFS) newRootDir(path string, info os.FileMode) *Dir {
	n := time.Now()
	return &Dir{
		attr: fuse.Attr{
			Inode: m.nextId(),
			Atime: n,
			Mtime: n,
			Ctime: n,
			// Crtime: n,
			Mode: info,
		},
		path:         path,
		fs:           m,
		autoHideList: make(goutensil.HashList),
	}
}

func (m *HideFS) newFile(path string, info os.FileInfo) *File {
	stat := info.Sys().(*syscall.Stat_t)
	return &File{
		attr: fuse.Attr{
			Valid:     500 * time.Millisecond,
			Inode:     m.nextId(),
			Size:      uint64(stat.Size),
			Blocks:    uint64(stat.Blocks),
			Atime:     time.Unix(stat.Atim.Sec, stat.Atim.Nsec),
			Mtime:     time.Unix(stat.Mtim.Sec, stat.Mtim.Nsec),
			Ctime:     time.Unix(stat.Ctim.Sec, stat.Ctim.Nsec),
			Mode:      os.ModeDir | info.Mode(),
			Nlink:     uint32(stat.Nlink),
			Uid:       stat.Uid,
			Gid:       stat.Gid,
			Rdev:      uint32(stat.Rdev),
			BlockSize: uint32(stat.Blksize),
			Flags:     0,
		},
		path: path,
		fs:   m,
	}
}

func (f *HideFS) Root() (fusefs.Node, error) {
	return f.root, nil
}

func (f *HideFS) Statfs(ctx context.Context, req *fuse.StatfsRequest, res *fuse.StatfsResponse) error {
	s := syscall.Statfs_t{}
	err := syscall.Statfs(f.path, &s)
	if err != nil {
		log.Println("DRIVE | Statfs syscall failed; ", err)
		return err
	}

	res.Blocks = s.Blocks
	res.Bfree = s.Bfree
	res.Bavail = s.Bavail
	res.Ffree = s.Ffree
	res.Bsize = uint32(s.Bsize)
	res.Files = s.Files
	res.Namelen = uint32(s.Namelen)
	res.Frsize = uint32(s.Frsize)
	// log.Println(res)

	return nil
}
