package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"sync"

	"bazil.org/fuse"
	fusefs "bazil.org/fuse/fs"
	_ "bazil.org/fuse/fs/fstestutil"
	"github.com/ilyakaznacheev/cleanenv"
	"gitlab.com/mb-saces/hidefs/goutensil"
	hidefs "gitlab.com/mb-saces/hidefs/internal"
)

func usage() {
	fmt.Fprintf(os.Stderr, "Usage of %s:\n", os.Args[0])
	fmt.Fprintf(os.Stderr, "  %s [-v] [-d] SOURCEDIR TARGETDIR [CONFIGFILE]\n", os.Args[0])
	fmt.Fprintf(os.Stderr, "  %s [-v] [-d] CONFIGFILE\n", os.Args[0])
	fmt.Fprintln(os.Stderr, "Mount removal helper:")
	fmt.Fprintf(os.Stderr, "  %s -u [-v] [-d] CONFIGFILE\n", os.Args[0])
	flag.PrintDefaults()
}

func main() {
	var cfgFileConfig ConfigFileConfig
	var cfgs HideFSConfigs

	flag.Usage = usage

	unmount := flag.Bool("u", false, "unmount everything in config file")
	verbose := flag.Bool("v", false, "be verbose")
	debugfuse := flag.Bool("d", false, "verbose fuse logging")

	flag.Parse()

	if *debugfuse {
		// TODO set fuse verbose
		fuse.Debug = func(msg interface{}) {
			fmt.Println(msg)
		}
	}

	if *unmount {
		if flag.NArg() != 1 || flag.Arg(0) == "" {
			usage()
			os.Exit(2)
		}

		err := cleanenv.ReadConfig(flag.Arg(0), &cfgs)
		goutensil.MaybePanic(err)

		for _, cfg := range cfgs.Configs {
			for _, targetDir := range cfg.TargetDirs {
				if *verbose {
					fmt.Printf("Unmount for: %s\n", targetDir)
				}
				err := fuse.Unmount(targetDir)
				if err != nil {
					fmt.Fprintf(os.Stderr, "Error while unmounting '%s': %v\n", targetDir, err)
				} else if *verbose {
					fmt.Printf("Unmounted '%s'\n", targetDir)
				}
			}
		}
		if *verbose {
			fmt.Println("Removal done. Bye.")
		}
		return
	}

	if flag.NArg() == 0 || flag.NArg() > 3 {
		usage()
		os.Exit(2)
	}

	// first determine the config filename
	switch flag.NArg() {
	case 1:
		cfgFileConfig.ConfigPath = flag.Arg(0)
	case 3:
		cfgFileConfig.ConfigPath = flag.Arg(3)
	}

	// load the config file
	if cfgFileConfig.ConfigPath != "" {
		err := cleanenv.ReadConfig(cfgFileConfig.ConfigPath, &cfgs)
		goutensil.MaybePanic(err)
	}

	// overwrite config with cmdline arguments
	if flag.NArg() > 1 {
		cfgs.Configs[0].SourceDir = flag.Arg(0)
		cfgs.Configs[0].TargetDirs = []string{flag.Arg(1)}
	}

	if len(cfgs.Configs) == 0 {
		fmt.Fprintln(os.Stderr, "No filesystem configured.")
		os.Exit(7)
	}

	// some sanity checks...
	for _, cfg := range cfgs.Configs {
		// hide list
		if len(cfg.HideNames) == 0 {
			fmt.Fprintln(os.Stderr, "WARNING: No hide list given. You may get a simple bind mount now. :)")
		} else if *verbose {
			fmt.Printf("INFO: HideList: %+v\n", cfg.HideNames)
		}

		// source dir
		if cfg.SourceDir == "" {
			fmt.Fprintln(os.Stderr, "Missing argument: No source dir given.")
			usage()
			os.Exit(3)
		}
		if exist, err := goutensil.DirectoryExists(cfg.SourceDir); !exist {
			if err != nil {
				fmt.Fprintf(os.Stderr, "Error while checking '%s': %s\n", cfg.SourceDir, err)
			} else {
				fmt.Fprintf(os.Stderr, "Source dir '%s' does not exist or is not a directory.\n", cfg.SourceDir)
			}
			os.Exit(4)
		}

		// target dirs
		if len(cfg.TargetDirs) == 0 {
			fmt.Fprintln(os.Stderr, "No target dirs given. Nothing to do.")
			os.Exit(5)
		}

		for _, targetDir := range cfg.TargetDirs {
			if exist, err := goutensil.DirectoryExists(targetDir); !exist {
				if err != nil {
					fmt.Fprintf(os.Stderr, "Error while checking '%s': %s\n", targetDir, err)
				} else {
					fmt.Fprintf(os.Stderr, "Target dir '%s' does not exist or is not a directory.\n", targetDir)
				}
				os.Exit(6)
			}
		}
		// TODO add src<->tgt overlap check
	}

	// reaching here means it seams not too broken...
	// so start this beast.

	var wg sync.WaitGroup
	for _, cfg := range cfgs.Configs {
		sourcefs := hidefs.NewHideFS(cfg.SourceDir, cfg.HideNames)

		for _, targetdir := range cfg.TargetDirs {
			wg.Add(1)
			go func(target string) {
				defer wg.Done()
				c, err := fuse.Mount(
					target,
					fuse.FSName("HideFS"),
					fuse.Subtype("hidefs"),
				)
				if err != nil {
					log.Fatal(err)
				}

				defer c.Close()

				err = fusefs.Serve(c, sourcefs)
				if err != nil {
					log.Fatal(err)
				}
			}(targetdir)
		}
	}
	wg.Wait()
}
