// goutensil
// Copyright (C) 2023  saces@c-base.org
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package goutensil

// HashList Simple Hashlist for fast lookups
type HashList map[string]struct{}

func (hl HashList) Add(s string) {
	hl[s] = struct{}{}
}

func (hl HashList) Contains(s string) bool {
	_, exists := hl[s]
	return exists
}

// HashList Simple Hashlist for fast lookups, ability to track unused items
type UsageHashList map[string]bool

func (uhl UsageHashList) Add(s string) {
	uhl[s] = false
}

func (uhl UsageHashList) Contains(s string) bool {
	_, exists := uhl[s]
	if exists {
		uhl[s] = true
	}
	return exists
}
