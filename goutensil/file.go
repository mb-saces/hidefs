package goutensil

import (
	"errors"
	"io/fs"
	"os"
	"path/filepath"
)

// FileExists checks if a file exists and is not a directory before we
// try using it to prevent further errors.
func FileExists(filename string) (bool, error) {
	info, err := os.Stat(filename)
	if err != nil {
		if errors.Is(err, fs.ErrNotExist) {
			return false, nil
		} else {
			return false, err
		}
	}
	return !info.IsDir(), nil
}

// DirectoryExists checks if a directory exists and is a directory before we
// try using it to prevent further errors.
func DirectoryExists(dirname string) (bool, error) {
	info, err := os.Stat(dirname)
	if err != nil {
		if errors.Is(err, fs.ErrNotExist) {
			return false, nil
		} else {
			return false, err
		}
	}
	return info.IsDir(), nil
}

// FileExists0600 checks if a file exists, is a regular file, and have permission 0600
func FileExists0600(filename string) (bool, error) {
	info, err := os.Stat(filename)
	if err != nil {
		if errors.Is(err, fs.ErrNotExist) {
			return false, nil
		} else {
			return false, err
		}
	}

	if info.IsDir() {
		return true, errors.New("is a directory")
	}

	if !info.Mode().IsRegular() {
		return true, errors.New("not a regular file")
	}

	if info.Mode().Perm()&^0o600 != 0 {
		return true, errors.New("permissions too wide")
	}

	return true, nil
}

// ConfigFileExist Checks if a (config) file exist.
//
// The first argument is a function like os.UserConfigDir followed by additional path elements. They are joined and
// tested for being a file.
//
// The function returns the resulting path, a bool that indicates existence, and an error value.
// You shuold not trust the path or bool flag if the error is not nil.
func ConfigFileExist(f func() (string, error), elems ...string) (path string, exist bool, err error) {
	path, err = f()
	if err != nil {
		return
	}
	path = filepath.Join(append([]string{path}, elems...)...)
	exist, err = FileExists(path)
	return
}
