package goutensil

import (
	"fmt"
	"os"
)

// MaybePanic convience wrapper for faster prototyping. Panics
// if err is not nil
func MaybePanic(err error) {
	if err != nil {
		panic(err)
	}
}

// MaybeExitRC convience wrapper for faster prototyping
func MaybeExitRC(err error, rc int) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "Fatal Error: %v\n", err)
		os.Exit(rc)
	}
}

// MaybeExit convience wrapper for faster prototyping
// exit with 1 if err is not nil
func MaybeExit(err error) {
	MaybeExitRC(err, 1)
}
