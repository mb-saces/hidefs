module gitlab.com/mb-saces/hidefs

go 1.19

require (
	bazil.org/fuse v0.0.0-20230120002735-62a210ff1fd5
	github.com/ilyakaznacheev/cleanenv v1.5.0
)

require (
	github.com/BurntSushi/toml v1.3.2 // indirect
	github.com/joho/godotenv v1.5.1 // indirect
	golang.org/x/sys v0.15.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
	olympos.io/encoding/edn v0.0.0-20201019073823-d3554ca0b0a3 // indirect
)
