package main

type ConfigFileConfig struct {
	ConfigPath string `env:"HIDEFS_CONFIG_FILE" env-description:"The path to the configuration file"`
	//ConfigType string `env:"HIDEFS_CONFIG_TYPE" env-default:"yaml" env-description:"The type of the configuration file"`
}

type HideFSConfigs struct {
	Configs []HideFSConfig `yaml:"HideFSConfig"`
}

type HideFSConfig struct {
	Name       string   `yaml:"Name"`
	HideNames  []string `yaml:"HideNames" env:"HIDEFS_HIDE_NAMES" env-default:".git:go.mod:go.sum" env-separator:":" env-description:"List of names the will never appear in the file system."`
	SourceDir  string   `yaml:"SourceDir" env:"HIDEFS_SOURCE_DIR" env-description:"The source dirctory"`
	TargetDirs []string `yaml:"TargetDirs" env:"HIDEFS_TARGET_DIRS" env-separator:":" env-description:"List of location the source directory is mounted to."`
}
